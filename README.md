# PC-Engine

based on https://archive.org/details/no-intro_romsets (Sega - 32X (20220501-212720))

ROM not added :
```sh
❯ tree -hs --dirsfirst                                                                         jeu. 02 févr. 2023 21:37:26
[   5]  .
├── [ 256]  [BIOS] 32X M68000 (USA).bin
├── [2.0K]  [BIOS] 32X SH-2 Master (USA).bin
└── [1.0K]  [BIOS] 32X SH-2 Slave (USA).bin

1 directory, 3 files
```
